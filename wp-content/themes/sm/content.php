<article id="post-<?php the_ID(); ?>" <?php post_class('article article--single'); ?>>

  <div class="container container--title">  
    <div class="container__column">
      <!-- title -->
      <h1 class="title title--h1 article__title--single">
        <?php the_title(); ?>
      </h1>
    </div>
  </div>

  <!-- wysiwyg -->
  <div class="wysiwyg wysiwyg--single">
    <?php the_content( null, true ); ?>
  </div>

</article>