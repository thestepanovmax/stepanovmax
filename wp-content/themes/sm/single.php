<?php get_header('single'); ?>

<div class="container container--breadcrumbs">  
  <div class="container__column">
    <?php the_breadcrumb(); ?>
  </div>
</div>

<?php
  while ( have_posts() ) : the_post();
    get_template_part( 'content' );
  endwhile;
?>

<?php get_footer(); ?>