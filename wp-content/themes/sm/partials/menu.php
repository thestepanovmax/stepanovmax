<!-- list -->
<ul class="list list--menu <?php echo (is_front_page() ? '' : 'list--menu-other'); ?>">
	<li class="list__item list__item--menu">
		<a href="/" class="list__link list__link--menu <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
			ГЛАВНАЯ
		</a>
	</li>
	<li class="list__item list__item--menu">
		<a href="/dev-app/" class="list__link list__link--menu <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
			КВАРТИРЫ ОТ ЗАСТРОЙЩИКОВ
		</a>
	</li>
	<li class="list__item list__item--menu">
		<a href="/building/" class="list__link list__link--menu <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
			СТРОИТЕЛЬСТВО
		</a>
	</li>
	<li class="list__item list__item--menu">
		<a href="/docs/" class="list__link list__link--menu <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
			ОФОРМЛЕНИЕ ДОКУМЕНТОВ
		</a>
	</li>
	<li class="list__item list__item--menu">
		<a href="/contacts/" class="list__link list__link--menu <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
			КОНТАКТЫ
		</a>
	</li>
</ul>