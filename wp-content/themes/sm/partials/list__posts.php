<?php 
	// array
	$postsAll = array(
		'post_type'=>'post',
		'post_status'=>'publish',
		'posts_per_page'=>-1
	);

	// the query
	$wpb_all_query = new WP_Query( $postsAll );
?>
 
<?php if ( $wpb_all_query->have_posts() ) : ?>

	<!-- widget -->
	<div class="widget widget--posts">

		<h3 class="title title--h4">
			<?php _e( 'Last articles' ); ?>
		</h3>

		<!-- list -->
		<ul class="list list--posts">
		 
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

				<li class="list__item list__item--posts">
					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_title(); ?>
					</a>
				</li>

			<?php endwhile; ?>
		
		</ul>
	 
		<?php wp_reset_postdata(); ?>
		
	</div>
	 
<?php else : ?>

	<?php get_template_part( 'content', 'none' ); ?>

<?php endif; ?>