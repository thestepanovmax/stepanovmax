<!-- language switcher -->
<?php if (function_exists('pll_the_languages')) { ?>
	<ul class="lang lang-<?php echo pll_current_language(); ?>">
		<?php
			pll_the_languages (
				array (
					'show_flags' => 0,
					'show_names' => 1
				)
			);
		?>
	</ul>
<?php } ?>
