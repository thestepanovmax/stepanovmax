<div class="share">
  <?php
    $titleWithoutSpaces = str_replace(' ', '&nbsp;', strip_tags(get_the_title()));
  ?>

  <?php // title ?>
  <h3 class="title title--h2">
    Не забудьте поделиться новыми объектами в соц.сетях или мессенджерах!
  </h3>

  <?php // list ?>
  <ul class="list list--social">
    
    <li class="list__item list__item--social">
      <a 	class="list__link--social"
        id="facebook"
        href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"
        onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
        title="Поделиться в Facebook"
        rel="nofollow">
        Поделиться в Facebook
      </a>
    </li>

    <li class="list__item list__item--social">
      <a 	class="list__link--social"
        id="twitter"
        href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php echo $titleWithoutSpaces; ?>&via=distillerytech"
        onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
        title="Поделиться в Twitter"
        rel="nofollow">
        Поделиться в Twitter
      </a>
    </li>

    <li class="list__item list__item--social">
      <a 	class="list__link--social"
        id="mail"
        target="_blank" href="mailto:?Subject=<?php echo $titleWithoutSpaces; ?>&amp;Body=<?php echo $titleWithoutSpaces; ?>%20-%20<?php the_permalink(); ?>"
        title="Поделиться в Почту"
        rel="nofollow">
        Поделиться в Почту
      </a>
    </li>

    <li class="list__item list__item--social">
      <a 	class="list__link--social"
        id="linkedin"
        href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php echo $titleWithoutSpaces; ?>&summary=<?php echo $titleWithoutSpaces; ?>"
        onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
        title="Поделиться в LinkedIn"
        rel="nofollow">
        Поделиться в LinkedIn
      </a>
    </li>

    <li class="list__item list__item--social">
      <script>
        // document.write(VK.Share.button(false,{type: "round", text: "Поделиться"}));
      </script>
    </li>

    <li class="list__item list__item--social">
      <div id="ok_shareWidget"></div>
      <script>
      !function (d, id, did, st, title, description, image) {
        var js = d.createElement("script");
        js.src = "https://connect.ok.ru/connect.js";
        js.onload = js.onreadystatechange = function () {
        if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
          if (!this.executed) {
            this.executed = true;
            setTimeout(function () {
              OK.CONNECT.insertShareWidget(id,did,st, title, description, image);
            }, 0);
          }
        }};
        d.documentElement.appendChild(js);
      }(document,"ok_shareWidget",document.URL,'{"sz":20,"st":"oval","ck":1}',"","","");
      </script>
    </li>
    
    <li class="list__item list__item--social">
      <a 	class="list__link--social"
        id="viber"
        href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"
        onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
        title="Поделиться в Viber"
        rel="nofollow">
        Поделиться в Viber
      </a>
      <script>
        var buttonID = "viber";
        var text = "Check this out: ";
        document.getElementById(buttonID)
          .setAttribute('href', "https://3p3x.adj.st/?adjust_t=u783g1_kw9yml&adjust_fallback=https%3A%2F%2Fwww.viber.com%2F%3Futm_source%3DPartner%26utm_medium%3DSharebutton%26utm_campaign%3DDefualt&adjust_campaign=Sharebutton&adjust_deeplink=" + encodeURIComponent("viber://forward?text=" + encodeURIComponent(text + " " + window.location.href)));
      </script>
    </li>

    <li class="list__item list__item--social">
      <a 	class="list__link--social" href="whatsapp://send?text=The text to share!" data-action="share/whatsapp/share">
        Поделиться в Whatsapp
      </a>
    </li>

  </ul>

</div>
