<?php // list ?>
<ul class="list list--contacts <?php echo (is_front_page() ? '' : 'list--contacts-other'); ?>">
  <li class="list__item list__item--contacts">
    <a href="tel:88002349171" class="list__link list__link--contacts <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
      8 (800) 234-91-71
    </a>
  </li>
  <li class="list__item list__item--contacts">
    <a href="mailto:info@an1tag.ru" class="list__link list__link--contacts <?php echo (is_front_page() ? '' : 'list__link--black'); ?>">
      info@an1tag.ru
    </a>
  </li>
</ul>