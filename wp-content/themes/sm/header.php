<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

  <title>
    <?php
      global $page, $paged;
      $title = wp_title('', false);
      echo $title;
    ?>
  </title>

  <meta name="yandex-verification" content="8bc1f43c2e4f9294" />
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <link rel='shortcut icon' type='image/x-icon' href='<?php echo get_template_directory_uri(); ?>/img/favicon.ico' />

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

  <!-- header -->
  <header class="header ">

    <!-- language switcher -->
    <?php if (function_exists('pll_the_languages')) { ?>
      <ul class="lang lang-<?php echo pll_current_language(); ?>">
        <?php
          pll_the_languages (
            array (
              'show_flags' => 0,
              'show_names' => 1
            )
          );
        ?>
      </ul>
    <?php } ?>

  </header>

  <!-- wrapper -->
  <main class="wrapper">