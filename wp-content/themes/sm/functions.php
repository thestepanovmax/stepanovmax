<?php


  // register menu
  function sm_theme_setup() {
    register_nav_menus(
      array( 
        'header' => 'Header menu'
      )
    );
  }
  add_action( 'after_setup_theme', 'sm_theme_setup' );


  // remove admin bar
  // add_filter( 'show_admin_bar', '__return_false' );


  // add css styles in header
  function add_my_stylesheet() {
    wp_enqueue_style( 'main-style', get_template_directory_uri() . '/build/css/all.css' );
  }
  add_action('wp_print_styles', 'add_my_stylesheet');


  // remove footer links
  add_action('init', 'tjnz_head_cleanup');
  function tjnz_head_cleanup() {
    remove_action( 'wp_head', 'feed_links_extra', 3 );                      // Category Feeds
    remove_action( 'wp_head', 'feed_links', 2 );                            // Post and Comment Feeds
    remove_action( 'wp_head', 'rsd_link' );                                 // EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' );                         // Windows Live Writer
    remove_action( 'wp_head', 'index_rel_link' );                           // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );              // previous link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );               // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );   // Links for Adjacent Posts
    remove_action( 'wp_head', 'wp_generator' );                             // WP version
    // REMOVE EMOJI ICONS
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');
    if (!is_admin()) {
      wp_deregister_script( 'wp-embed' );
      wp_deregister_script('jquery');                                     // De-Register jQuery
      wp_register_script('jquery', '', '', '', true);                     // Register as 'empty', because we manually insert our script in header.php
    }
  }


  // page pagination
  function page_navigation() {
    global $wp_query;  
    $big = 999999999; // unique number for change  
    $args = array(
      'base'        => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
      'format'      => '',
      'current'     => max( 1, get_query_var('paged') ),
      'total'       => $wp_query->max_num_pages,
      'prev_text'   => __(''),
      'next_text'   => __('')
    );  
    $result = paginate_links( $args );  
    // remove pagination for the 1st page
    $result = str_replace( '/page/1/', '', $result );  
    echo $result;
  }


  // Allow SVG files to be attached
  add_filter( 'upload_mimes', 'my_myme_types', 1, 1 );
  function my_myme_types( $mime_types ) {
    // Adding .svg extension
    $mime_types['svg'] = 'image/svg+xml';
    // Adding .json extension
    $mime_types['json'] = 'application/json';
    // Adding .ai extension
    $mime_types['ai'] = 'image/vnd.adobe.illustrator';
    // Adding .psd extension
    $mime_types['psd'] = 'image/vnd.adobe.photoshop';
    // Remove .xls extension
    unset( $mime_types['xls'] );
    // Remove .xlsx extension
    unset( $mime_types['xlsx'] );
    return $mime_types;
  }


  function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
  }
  add_action('get_header', 'remove_admin_login_header');


  /*=============================================
  =            BREADCRUMBS                      =
  =============================================*/
  //  to include in functions.php
  function the_breadcrumb() {
    $sep = ' > ';
    if (!is_front_page()) {
    
      // Start the breadcrumb with a link to your homepage
      echo '<div class="breadcrumbs">';
      echo '<a href="';
      echo get_option('home');
      echo '">';
      bloginfo('name');
      echo '</a>' . $sep;

      // Check if the current page is a category, an archive or a single page. If so show the category or archive name.
      if (is_category() || is_single() ){
        the_category('title_li=');
      } elseif (is_archive() || is_single()){
        if ( is_day() ) {
          printf( __( '%s', 'text_domain' ), get_the_date() );
        } elseif ( is_month() ) {
          printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
        } elseif ( is_year() ) {
          printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
        } else {
          _e( 'Blog Archives', 'text_domain' );
        }
      }

      // If the current page is a single post, show its title with the separator
      if (is_single()) {
        echo $sep;
        the_title();
      }

      // If the current page is a static page, show its title.
      if (is_page()) {
        echo the_title();
      }

      // if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
      if (is_home()){
        global $post;
        $page_for_posts_id = get_option('page_for_posts');
        if ( $page_for_posts_id ) { 
          $post = get_page($page_for_posts_id);
          setup_postdata($post);
          the_title();
          rewind_posts();
        }
      }

      echo '</div>';
    }
  }


  // creating page with general settings of the website
  if( function_exists('acf_add_options_page') ) {
    $option_page = acf_add_options_page(
      array(
        'page_title' => 'General settings page',
        'menu_title' => 'General settings',
        'menu_slug' => 'general-settings',
        'capability' => 'edit_posts',
        'position' => 2,
        'update_button'		=> __('Update general settings', 'acf'),
        'updated_message'	=> __('General settings has been successfully updated', 'acf'),
        'redirect' => false
      )
    );
  }

  // prevent wordpress from rendering line breaks as br tags
  remove_filter('the_content', 'wpautop');

?>