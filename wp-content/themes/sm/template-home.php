<?php
  // Template Name: Home page
  get_header();

  // variables
  $image_main = get_field('image_main');
  $image_main_url = $image_main['url'];
  $image_main_alt = $image_main['alt'];
?>

<!-- container -->
<div class="container container--home">

  <!-- img -->
  <img class="img img--home" src="<?php echo get_template_directory_uri(); ?>/build/img/hi.svg" alt="Привет, мой друг!">

  <?php 
    // the query
    $the_query = new WP_Query(
      array(
        'posts_per_page' => 3,
      )
    ); 
  ?>

  <?php if ( $the_query->have_posts() ) : ?>

    <!-- list -->
    <ul class="list list--vertical list--home">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php
          $excerpt = get_field('excerpt');
        ?>

        <li class="list__item list__item--home">
          <article class="article">
            <h3 class="article__title article__title--mini">
              <a class="link link--home" href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
              </a>
            </h3>
            <div class="article__excerpt-wysiwyg">
              <?php echo $excerpt; ?>
            </div>
          </article>
        </li>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>

    </ul>

  <?php else : ?>
    <p><?php __('No News'); ?></p>
  <?php endif; ?>

</div>

<?php get_footer(); ?>