"use strict";

// including libraries
import $ from 'jquery';
import scrollTo from 'jquery.scrollto';
import hljs from 'highlightjs';

// including custom scripts
import preloader from './preloader';
import highlightCode from './highlightCode';
import scrollToElement from './scrollToElement';

// calling of the custom scripts 
preloader($);
highlightCode($, hljs);
scrollToElement($, scrollTo);