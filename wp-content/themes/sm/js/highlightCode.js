export default function ($, hljs) {

  hljs.configure(
    {
      languages: 'python'
    }
  );


  hljs.initHighlightingOnLoad();
}