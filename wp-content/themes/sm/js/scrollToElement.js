export default function ($, scrollTo) {

  const linkWithHash = $('a[href^="#"]');

  // Scroll the window, stop any previous animation, stop on user manual scroll
  const scrollWindow = (e) => {
    $(window).stop(true).scrollTo(
      e.hash, {
        duration: 500,
        interrupt: true
      }
    );
  }

  // Bind to the click of all links with a #hash in the href
  linkWithHash.on('click',
    (e) => {
      // Prevent the jump and the #hash from appearing on the address bar
      e.preventDefault();
      const targetLink = e.target;
      scrollWindow(targetLink);
    }
  );

}