<?php get_header('other'); ?>

  <div class="container container--desktop">

    <?php if ( have_posts() ) : ?>

        <!-- set of posts -->
        <div class="set-posts set-posts--short">

          <!-- list -->
          <ul class="list list--vertical list--articles-list">

            <?php while ( have_posts() ) : the_post(); ?>
              
              <li class="list__item list__item--articles-list">
                <a class="link" href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </li>

            <?php endwhile; ?>

            <?php the_posts_pagination(); ?>

          </ul>

        </div>
        
      <?php else : get_template_part( 'content', 'none' ); ?>

    <?php endif; ?>

  </div>

<?php get_footer(); ?>
