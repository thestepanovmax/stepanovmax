<?php
  // Template Name: Telegram post
  get_header();
?>

<div class="container container--breadcrumbs">  
  <div class="container__column">
    <?php the_breadcrumb(); ?>
  </div>
</div>

<!-- container -->
<div class="container container--home">

  <!-- img -->
  <img class="img img--home" src="<?php echo get_template_directory_uri(); ?>/build/img/404.svg">

</div>

<?php get_footer(); ?>