<?php
  // Template Name: Home page
  get_header();

  // variables
  $image_main = get_field('image_main');
  $image_main_url = $image_main['url'];
  $image_main_alt = $image_main['alt'];
?>

<div class="container container--breadcrumbs">  
  <div class="container__column">
    <?php the_breadcrumb(); ?>
  </div>
</div>

<!-- container -->
<div class="container container--archive">

  <!-- img -->
  <img class="img img--home" src="<?php echo $image_main_url; ?>" alt="<?php echo $image_main_alt; ?>">

  <!-- list -->
  <ul class="list list--vertical list--home">

    <?php while ( have_posts() ) : the_post();?>

      <?php
        $excerpt = get_field('excerpt');
      ?>

      <li class="list__item list__item--home">
        <article class="article">
          <h3 class="article__title article__title--mini">
            <a class="link link--home" href="<?php the_permalink(); ?>">
              <?php the_title(); ?>
            </a>
          </h3>
          <div class="article__excerpt-wysiwyg">
            <?php echo $excerpt; ?>
          </div>
        </article>
      </li>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

  </ul>

</div>

<?php get_footer(); ?>