<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?>>

  <div class="article__row article__row--full">

    <div class="article__full">

      <!-- wysiwyg -->
      <div class="wysiwyg wysiwyg--single">
        <?php the_content( null, true ); ?>
      </div>

    </div>

  </div>
  
</article>
