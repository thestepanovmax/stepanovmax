<?php
  // Template Name: Telegram post
  get_header();
?>

<div class="container container--breadcrumbs">  
  <div class="container__column">
    <?php the_breadcrumb(); ?>
  </div>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class('article article--single'); ?>>

  <div class="container container--title">  
    <div class="container__column">
      <!-- title -->
      <h1 class="title title--h1 article__title--single">
        <?php the_title(); ?>
      </h1>
    </div>
  </div>

  <!-- wysiwyg -->
  <div class="wysiwyg wysiwyg--single">

    <div class="container container--single">
      <div class="container__column container__column--33">

        <h3 class="title title--h2">
          Оглавление
        </h3>

        <ul class="list list--vertical">
          <li class="list__item">
            <a class="link" href="#font-icons">
              Шрифтовые иконки
            </a>
          </li>

          <li class="list__item">
            <a class="link" href="#svg">
              SVG спрайты
            </a>
          </li>
        </ul>

      </div>
      <div class="container__column container__column--67">

        <h3 class="title title--h2">
          Введение
        </h3>

        <p>Я очень люблю векторную графику и всегда, когда подворачивается случай, использую её по назначению. Особенно в шрифтовых иконках, что в последнее время стало очень популярным. Надеюсь, никто не станет спорить, что в вопросе иконок вектор гораздо круче растра. Вот и сейчас у меня возникла задача создания иконка Telegram для шрифтовых иконок.</p>

        <p>Дабы не тратить время на её создание, я решил поискать готовый вариант в SVG или AI форматах, но к великому своему удивлению не нашёл оных. Иконки сами по себе есть и в нужных форматах, и совершенно разные, но среди них нужной для меня не было.</p>

      </div>
    </div>


    <div class="container container--single">
      <div class="container__column container__column--33">

        <p>Вот, что мне удалось найти:</p>

        <img class="aligncenter wp-image-752 size-full" src="http://stepanovmax.local/wp-content/uploads/2016/09/results.jpg" alt="Иконка Telegram" width="800" height="300" />

      </div>
      <div class="container__column container__column--67">

        <p>А это немного не то, что мне хотелось найти. В итоге я взял за основу голубую иконку и на основе неё у меня получилась векторная иконка Telegram. В принципе дело в целом нехитрое, но так уж случается, если что-то вдруг надобится, то его днём с огнём не найдёшь.</p>

      </div>
    </div>


    <div class="container container--single" id="font-icons">
      <div class="container__column container__column--50">

        <h2 class="title title--h2">
          Шрифтовые иконки
        </h2>

        <p>По ссылке далее вы можете взять себе уже готовую иконку для импорта в шрифт в <a class="link" href="http://stepanovmax.local/wp/wp-content/uploads/2016/09/telegram-ai-svg.zip">svg-формате</a>. А вот по этой ссылке вы можете взять себе уже готовый набор для <a class="link" href="http://stepanovmax.local/wp/wp-content/uploads/2016/09/fontello-4fd56199.zip">сервиса Fontello</a>.</p>

      </div>
    </div>


    <div class="container container--title" id="svg">
      <div class="container__column container__column--50">

        <h2 class="title title--h2">
          SVG спрайты
        </h2>

      </div>
    </div>


    <div class="container container--single" id="svg">
      <div class="container__column container__column--50">

        <p>Хочу отметить, что гораздо удобнее подключать на сайт иконки через svg спрайт, а не через шрифт или через svg файлы. Метод создания очень прост: - создаёте или находите нужную иконку в svg формате (найти можно, например, на очень содержательном сайте - flaticon), и грузите ее на сайт icomoon.</p>

        <p>Эту иконку я нарисовал сам и залил её на icomoon. Вот что у меня получилось:</p>

        <p>Чтобы применить иконку на сайте, надо взять содержание svg файла и вставить куда-нибудь сразу после body. А чтобы вывести иконку в нужном месте, нужно взять код вывода иконки и вставить туда, куда нужно. И всё.</p>

        <p>Прекрасным свойством является то, что вы можете перекрашивать иконку по ховеру или в статичном положении, добавив svg селектору свойство fill.</p>

      </div>
      <div class="container__column container__column--50">

        <h4 class="title title--h4">
          HTML вывода иконки
        </h4>

<pre>
<code class="html">&lt;svg class="icon icon-telegram"&gt;
  &lt;use xlink:href="#icon-telegram"&gt;
  &lt;/use&gt;
&lt;/svg&gt;
</code>
</pre>

        <br class="">

        <h4 class="title title--h4">
          Содержание svg файла
        </h4>

<pre>
<code class="html">&lt;svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"&gt;
  &lt;defs&gt;
    &lt;symbol id="icon-telegram" viewBox="0 0 32 32"&gt;
      &lt;path class="path1" d="M29.638 2.522l-28.397 11.136c0 0-1.344 0.461-1.235 1.306 0.109 0.851 1.203 1.235 1.203 1.235l7.142 2.406 17.152-10.893c0 0 0.992-0.602 0.954 0 0 0 0.179 0.109-0.352 0.602s-13.523 12.262-13.523 12.262l-0.883 7.814c0.397 0.173 0.755-0.102 0.755-0.102l4.64-4.23 7.168 5.536c1.946 0.851 2.65-0.922 2.65-0.922l5.056-25.498c0.006-1.677-2.33-0.653-2.33-0.653z"&gt;&lt;/path&gt;
    &lt;/symbol&gt;
  &lt;/defs&gt;
&lt;/svg&gt;
</code>
</pre>

      </div>

    </div>

  </div>

</article>

<?php get_footer(); ?>