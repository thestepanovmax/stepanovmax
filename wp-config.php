<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stepanovmax');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sEh,z <516q_F}!;=oHQ4+)l*0zpd?G$px~F!xX{8&nn0dd@3^^A8/^fu0SvKei~');
define('SECURE_AUTH_KEY',  'Ir)?-1ZOsB#c_2.TfegCUbVTJ1>kk2*bVZ/G.]<yi[^D?S.DcEZtcomRX^5`3<]Z');
define('LOGGED_IN_KEY',    'iDF,==*H{ ~ZTH1>Z1d]~ jOxESh;T-ULVPA4CSeKlSdy?4!G8Y(c$5NUgR-[:?4');
define('NONCE_KEY',        'dfDvt3%fE8^EB 8slhkFV+r%?#TD9U0^{$>Xyg9[uI^R,j,?W!~d-oPm)?S&N-i&');
define('AUTH_SALT',        'APkr>dvb`79,8aEf]%}59sL:/M)w_R*Hqz~:Xm?Ig?yMrk09w;Ts_1>Ln lk9tbl');
define('SECURE_AUTH_SALT', '@}]U#}aV]D$g*q<gN-dio;e-r?Hd9!kCU&-DaVBQDz(|Y,:u-(^uEBn{QBKC@~!P');
define('LOGGED_IN_SALT',   'o}cG<gsWdNWl=J8pOD)nc7_^@bUL-_OVcL8Q-(u!%M2W?s]S~-{XO#Gg]M&niw3)');
define('NONCE_SALT',       'P1h(TIWl2~kip[kZH$izy0?2zFM<v`>u zGs`Cxa9*IFDYx1|9L!G9>.wi)Ev^V{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'che_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
