/* ==========================================================
 * START OF THE IMPORT
 * ==========================================================
 */
import gulp from 'gulp';
import less from 'gulp-less';
import util from 'gulp-util';
import babel from 'gulp-babel';
import watch from 'gulp-watch';
import babelify from 'babelify';
import notify from 'gulp-notify';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import plumber from 'gulp-plumber';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import babelRegister from 'babel-register';
import autoprefixer from 'gulp-autoprefixer';

import buffer from 'vinyl-buffer';
import source from 'vinyl-source-stream';

import glob from 'glob';
import browserify from 'browserify';
import browserSync from 'browser-sync';
import runSequence from 'run-sequence';
import mainNpmFiles from 'main-npm-files';

/* ==========================================================
 * END OF THE IMPORT
 * ==========================================================
 */


/* ==========================================================
 * START OF THE VARIABLES
 * ==========================================================
 */

const PATH_THEME = './wp-content/themes/sm/';
const NPM_FILES_CSS = mainNpmFiles('**/*.css');

const CONFIG = {
  PATH: {
    SRC: {
      JS: PATH_THEME + 'js/index.js',
      CSS: PATH_THEME + 'less/index.less',
      IMG: PATH_THEME + 'img/**/*'
    },
    DEST: {
      JS: PATH_THEME + 'build/js/',
      CSS: PATH_THEME + 'build/css/',
      IMG: PATH_THEME + 'build/img/'
    },
    WATCH: {
      JS: PATH_THEME + 'js/**/*',
      CSS: PATH_THEME + 'less/**/*',
      IMG: PATH_THEME + 'img/**/*'
    }
  },
  FILENAME: {
    JS: 'all.js',
    CSS: 'all.css'
  },
  PREFIXES: {
    browsers: [
      'ie >= 11',
      'Firefox >= 45',
      'ios >= 9'
    ]
  },
  RULES_CLEAN_CSS: {
    1: {
      all: true,
      normalizeUrls: false
    },
    2: {
      restructureRules: true
    }
  }
};

/* ==========================================================
 * END OF THE VARIABLES
 * ==========================================================
 */


/* ==========================================================
 * START OF THE TASKS
 * ==========================================================
 */

gulp.task(
  'default',
  ['build-local']
);

gulp.task(
  'build-local',
  [
    'less-local',
    'js-local',
    'img-local',
    'watch'
  ]
);

gulp.task(
  'build-dev',
  [
    'less-local',
    'js-dev',
    'img-local'
  ]
);

/* ==========================================================
 * END OF THE TASKS
 * ==========================================================
 */


/* ==========================================================
 * START OF THE LOCAL ASSEMBLY
 * ==========================================================
 */

// local env: less compilation
gulp.task(
  'less-local',
  () =>
    gulp.src([
      CONFIG.PATH.SRC.CSS
    ])
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(autoprefixer(CONFIG.PREFIXES))
      .on(
        'error',
        notify.onError(error =>
          `An error occurred while making CSS for public js. Look in the console for details. ${error}`)
      )
      .pipe(cleanCSS({
        level: CONFIG.RULES_CLEAN_CSS
      }))
      .pipe(concat(CONFIG.FILENAME.CSS))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(CONFIG.PATH.DEST.CSS))
);


// local env: js compilation
gulp.task(
  'js-local',
  () => {
    return browserify({
      entries: CONFIG.PATH.SRC.JS,
      debug: true
    })
      .transform(
        babelify.configure({
          presets: ['env']
        })
      )
      .bundle()
      .pipe(source(CONFIG.FILENAME.JS))
      .on(
        'error',
        notify.onError(error =>
          `An error occurred while making JS for public. Look in the console for details. ${error}`)
      )
      .pipe(gulp.dest(CONFIG.PATH.DEST.JS));
  }
);


// local env: js compilation
gulp.task(
  'js-dev',
  () => {
    return browserify({
      entries: CONFIG.PATH.SRC.JS,
      debug: false
    })
      .transform(
        babelify.configure({
          presets: ['env']
        })
      )
      .bundle()
      .pipe(source(CONFIG.FILENAME.JS))
      .on(
        'error',
        notify.onError(error =>
          `An error occurred while making JS for public. Look in the console for details. ${error}`)
      )
      .pipe(buffer())
      .pipe(uglify())
      .pipe(gulp.dest(CONFIG.PATH.DEST.JS));
  }
);


// local env: copying of images
gulp.task(
  'img-local',
  () => {
    gulp.src(CONFIG.PATH.SRC.IMG)
      .pipe(gulp.dest(CONFIG.PATH.DEST.IMG));
  }
);


// local env: watching for the files changing
gulp.task(
  'watch',
  () => {
    gulp.watch(CONFIG.PATH.WATCH.CSS, ['less-local']);
    gulp.watch(CONFIG.PATH.WATCH.JS, ['js-local']);
    gulp.watch(CONFIG.PATH.WATCH.IMG, ['img-local']);
  }
);

/* ==========================================================
 * END OF THE LOCAL ASSEMBLY
 * ==========================================================
 */